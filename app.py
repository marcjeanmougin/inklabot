from flask import Flask, request
import requests
import re
import json
import tempfile
import gitlab
import variables

app = Flask(__name__)

project = variables.PROJECT
allowed_headers = variables.ALLOWED_HEADERS
gl = gitlab.Gitlab(url="https://gitlab.com", private_token=variables.TOKEN)
p = gl.projects.get(project)

@app.route("/submit", methods=["POST"])
def submit():
    # check token
    print(request.headers)
    if request.headers.get("X-Gitlab-Token") not in allowed_headers:
        return "", 403
    try:
        # get file
        b = json.loads(request.data)
        print(b)
        if 'issue' not in b or b['issue']['iid'] not in variables.BRANCHES:
            if 'merge_request' not in b:
                return "", 200
            else:
                mr = p.merge_requests.get(b['merge_request']['iid'])
                if mr.source_branch.endswith("-translations") or mr.target_branch not in variables.BRANCHES.values():
                    return "", 200
                changes = mr.changes()["changes"]
                relevant = False
                for x in changes:
                    if x["new_path"].startswith("po/"):
                        relevant = True
                        break
                if relevant:
                    mr.notes.create({'body': 'Hi! :wave: \n\n We notice that you are likely trying to update some translations directly :) Just make sure that your work does not conflict with whatever happens in the '+mr.target_branch+'-translations branch. \n\n If you have a conflict with it, the preferred way would be to just attach the newer po file into the right issue('+str(variable.BRANCHES)+'). If not, by all means, go ahead, I\'m just a bot\n\n Thanks! :rocket:'})
                return "", 200

        issue = p.issues.get(b['issue']['iid'])
        m = re.search(r'\[([^/]*\.po)\]\((.*)\)', b["object_attributes"]["note"])
        print(b["object_attributes"]["note"])
        if not m:
            return "", 200
        r = requests.get("https://gitlab.com/-/project/"+str(project)+m.group(2), allow_redirects=True)
        # API commit
        data = {'branch': variables.BRANCHES[]+"-translations",
                'commit_message': 'update translations ('+m.group(1)[:-3]+')',
                'author_name': b['user']['name'],
                'author_email': str(b['user']['id'])+'-'+b['user']['username']+'@users.noreply.gitlab.com',
                'actions' : [{'action': 'update',
                              'file_path': 'po/'+m.group(1),
                              'content': r.content}]
                }
        print(data)
        commit = p.commits.create(data)
        print(commit)
        note = issue.notes.get(b['object_attributes']['id'])
        note.awardemojis.create({'name': 'rocket'})
        return "", 200

        # message on fail
    except:
        return "", 200


